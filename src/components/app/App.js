import React from 'react';
import './App.css';

import { BrowserRouter } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import { useGlobalContext } from '../global_context/GlobalContext';

import Header from '../header/Header';
import Body from '../body/Body';

const history = createBrowserHistory();

const App = () => {
    const globalContext = useGlobalContext();

    return (
        <BrowserRouter history={history}>
            <div id={"app"} className={globalContext.currentTheme}>
                <Header />
                <Body />
            </div>
        </BrowserRouter>
    );
};

export default App;
