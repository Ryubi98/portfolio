import React from 'react';
import './ToggleNavbar.css';

const ToggleNavbar = ({ isOpen, onClick }) => {
    return (
        <div className={"header-toggle-navbar" + (isOpen ? " is-open" : "")}>
            <div className={"header-toggle-navbar-block"} onClick={onClick}>
                <span className={"header-toggle-navbar-icon"} />
            </div>
            <span className={"header-toggle-navbar-icon-mask"} onClick={onClick} />
        </div>
    );
};

export default ToggleNavbar;
