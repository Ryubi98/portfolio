import React from 'react';
import './SubItem.css';

import { useHistory } from 'react-router-dom';

const SubItem = ({ children, className, to, isIntern }) => {
    const history = useHistory();
    const click = () => {
        if (to) {
            if (isIntern) {
                history.push(to);
            }
            else {
                window.open(to, '_newtab');
            }
        }
    };

    return (
        <div className={"header-navbar-sub-item " + className + (to ? " link" : "")} onClick={click}>
            {children}
        </div>
    );
};

export default SubItem;
