import React from 'react';
import './SubItems.css';

const SubItems = ({ children, isOpen = false }) => {
    return (
        <div className={"header-navbar-sub-items" + (isOpen ? " is-open" : "")}>
            {children}
        </div>
    );
};

export default SubItems;
