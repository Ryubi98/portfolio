import React from 'react';
import './Item.css';

import { useHistory, useLocation } from 'react-router-dom';

const Item = ({ children, isOpen = false, onClick = () => {}, to, isIntern }) => {
    const history = useHistory();
    const location = useLocation();

    const click = () => {
        if (to) {
            if (isIntern) {
                history.push(to);
            }
            else {
                window.open(to, '_newtab');
            }
        }
        onClick();
    };

    return (
        <div className={"header-navbar-item" + (isOpen ? " is-open" : "") + (to ? " link" : "")} onClick={click}>
            {children}
            <div className={"header-navbar-item-rule" + (location.pathname === to || isOpen ? " is-open" : "")}>
                <span />
            </div>
        </div>
    );
};

export default Item;
