import React from 'react';
import './TextIcon.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FormattedMessage } from 'react-intl';

const TextIcon = ({ icon, id, onClick }) => {
    return (
        <p className={"text-icon-link"} onClick={onClick}>
            <FontAwesomeIcon icon={icon} /><FormattedMessage id={id} />
        </p>
    );
};

export default TextIcon;
