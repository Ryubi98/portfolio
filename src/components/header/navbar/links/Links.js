import React, {useState} from 'react';
import './Links.css';

import { faLink } from '@fortawesome/free-solid-svg-icons';
import { faLinkedinIn, faGitlab, faGithub } from '@fortawesome/free-brands-svg-icons';

import Item from '../item/Item';
import SubItems from '../item/sub-items/SubItems';
import SubItem from '../item/sub-items/sub-item/SubItem';
import TextIcon from '../text-icon/TextIcon';

const Links = () => {
    const [ isOpen, setIsOpen ] = useState(false);

    return (
        <div className={"header-navbar-links"} onMouseEnter={() => setIsOpen(true)} onMouseLeave={() => setIsOpen(false)}>
            <Item isOpen={isOpen}>
                <TextIcon id={"header.links"} icon={faLink}/>
            </Item>
            <SubItems isOpen={isOpen}>
                <SubItem className={"sub-item-text"} to={"https://www.linkedin.com/in/antonin-ginet"}>
                    <TextIcon icon={faLinkedinIn} id={"header.linked-in"} />
                </SubItem>
                <SubItem className={"sub-item-text"} to={"https://gitlab.com/Ryubi98"}>
                    <TextIcon icon={faGitlab} id={"header.gitlab"} />
                </SubItem>
                <SubItem className={"sub-item-text"} to={"https://github.com/Ryubi98"}>
                    <TextIcon icon={faGithub} id={"header.github"} />
                </SubItem>
            </SubItems>
        </div>
    );
};

export default Links;
