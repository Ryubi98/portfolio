import React from 'react';
import './ThemeSwitch.css';

import { useGlobalContext } from '../../../global_context/GlobalContext';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSun, faMoon } from '@fortawesome/free-solid-svg-icons';

const icons = {
    "light": faSun,
    "dark": faMoon
};

const ThemeSwitch = () => {
    const globalContext = useGlobalContext();

    return (
        <div className={"theme-switch"}>
            <div className={"theme-switch-button"} onClick={globalContext.changeTheme}>
                <div className={"theme-switch-toggle"}>
                    <FontAwesomeIcon className={"theme-switch-toggle-logo"} icon={icons[globalContext.currentTheme]} />
                </div>
            </div>
        </div>
    );
};

export default ThemeSwitch;
