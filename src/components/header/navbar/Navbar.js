import React from 'react';
import './Navbar.css';

import { faHome, faSmile, faUserGraduate, faPencilAlt } from '@fortawesome/free-solid-svg-icons';

import Item from './item/Item';
import Translation from './translation/Translation';
import Links from './links/Links';
import TextIcon from './text-icon/TextIcon';
import ThemeSwitch from './theme-switch/ThemeSwitch';

const Navbar = ({ isOpen, onClick }) => {
    return (
        <nav className={"header-navbar" + (isOpen ? " is-open" : "")}>
            <Item to={"/"} isIntern onClick={onClick}>
                <TextIcon icon={faHome} id={"header.home"} />
            </Item>
            <Item to={"/about-me"} isIntern onClick={onClick}>
                <TextIcon icon={faSmile} id={"header.about-me"} />
            </Item>
            <Item to={"/education"} isIntern onClick={onClick}>
                <TextIcon icon={faUserGraduate} id={"header.education"} />
            </Item>
            <Item to={"/projects"} isIntern onClick={onClick}>
                <TextIcon icon={faPencilAlt} id={"header.projects"} />
            </Item>
            <Links />
            <Translation onClick={onClick}/>
            <ThemeSwitch />
        </nav>
    );
};

export default Navbar;
