import React, { useState } from 'react';
import './Translation.css';

import { useGlobalContext } from '../../../global_context/GlobalContext';

import FRFlag from './images/fr.svg';
import UKFlag from './images/uk.svg';

import Flag from './flag/Flag';
import SubItems from '../item/sub-items/SubItems';
import SubItem from '../item/sub-items/sub-item/SubItem';
import Item from '../item/Item';

const Translation = ({ onClick }) => {
    const [ isOpen, setIsOpen ] = useState(false);
    const globalContext = useGlobalContext();

    const flags = {
        "fr": {
            src: FRFlag,
            alt: globalContext.languageFile["header.alt-flag-fr"]
        },
        "en": {
            src: UKFlag,
            alt: globalContext.languageFile["header.alt-flag-en"]
        }
    };

    const selectFlag = (language) => {
        onClick();
        setIsOpen(false);
        globalContext.changeLanguage(language);
    };

    return (
        <div className={"header-navbar-translation"} onMouseEnter={() => setIsOpen(true)} onMouseLeave={() => setIsOpen(false)}>
            <Item isOpen={isOpen}>
                <Flag flag={flags[globalContext.currentLanguage]} className={"header-navbar-translation-flag"} />
            </Item>
            <SubItems isOpen={isOpen}>
                <SubItem className={"sub-item-img"}>
                    <Flag flag={flags["fr"]} className={"header-navbar-translation-flag can-click"} onClick={() => selectFlag("fr")} />
                </SubItem>
                <SubItem className={"sub-item-img"}>
                    <Flag flag={flags["en"]} className={"header-navbar-translation-flag can-click"} onClick={() => selectFlag("en")} />
                </SubItem>
            </SubItems>
        </div>
    );
};

export default Translation;
