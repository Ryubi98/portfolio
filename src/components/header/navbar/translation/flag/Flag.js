import React from 'react';

const Flag = ({ flag, className, onClick }) => {
    return (
        <img src={flag.src} alt={flag.alt} className={className} onClick={onClick} />
    );
};

export default Flag;
