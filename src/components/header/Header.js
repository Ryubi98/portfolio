import React, { useState } from 'react';
import './Header.css';

import Container from '../container/Container';
import Title from './title/Title';
import Navbar from './navbar/Navbar';
import ToggleNavbar from './toggle-navbar/ToggleNavbar';

const Header = () => {
    const [ isOpen, setIsOpen ] = useState(false);

    return (
        <header>
            <Container>
                <div className={"header"}>
                    <Title id={"header.title"} />
                    <Navbar isOpen={isOpen} onClick={() => setIsOpen(false)} />
                    <ToggleNavbar isOpen={isOpen} onClick={() => setIsOpen(!isOpen)} />
                </div>
            </Container>
        </header>
    );
};

export default Header;
