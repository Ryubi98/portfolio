import React from 'react';
import './Title.css';

import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

const Title = ({ id }) => {
    return (
        <Link to={"/"} className={"header-title"}>
            <h1>
                <FormattedMessage id={id} />
            </h1>
        </Link>
    );
};

export default Title;
