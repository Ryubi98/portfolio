import React, { createContext, useContext, useReducer } from 'react';

import { IntlProvider } from 'react-intl';

import fr from './translations/fr.json';
import en from './translations/en.json';

const messages = {
    "fr": fr,
    "en": en
};

const init = () => {
    const language = localStorage.getItem("currentLanguage") ?? "fr";
    const theme = localStorage.getItem("currentTheme") ?? "light";

    return {
        currentLanguage: language,
        languageFile: messages[language],

        currentTheme: theme
    };
};

const GlobalContext = createContext(init());

const globalReducer = (state, action) => {
    let newState;

    switch (action.type) {
        case "CHANGE_LANGUAGE":
            newState = {
                ...state,
                currentLanguage: action.language,
                languageFile: messages[action.language]
            };
            localStorage.setItem("currentLanguage", newState.currentLanguage);
            break;
        case "CHANGE_THEME":
            newState = {
                ...state,
                currentTheme: state.currentTheme === "light" ? "dark" : "light"
            }
            localStorage.setItem("currentTheme", newState.currentTheme);
            break;
        default:
            return state;
    }

    return newState;
};

const useGlobalContext = () => {
    return useContext(GlobalContext);
};

const GlobalContextProvider = ({ children }) => {
    const [ value, dispatch ] = useReducer(globalReducer, useGlobalContext());

    const changeLanguage = (language) => {
        dispatch({ type: "CHANGE_LANGUAGE", language: language });
    };

    const changeTheme = () => {
        dispatch({ type: "CHANGE_THEME" });
    };

    return (
        <GlobalContext.Provider value={{ ...value, changeLanguage: changeLanguage, changeTheme: changeTheme }}>
            <IntlProvider locale={value.currentLanguage} messages={messages[value.currentLanguage]}>
                {children}
            </IntlProvider>
        </GlobalContext.Provider>
    );
};

export default GlobalContextProvider;
export { useGlobalContext };
