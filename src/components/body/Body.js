import React, { useEffect } from 'react';
import './Body.css';

import { Switch, Route, Redirect, useLocation } from 'react-router-dom';

import Container from '../container/Container';
import Home from '../body/sections/home/Home';
import AboutMe from '../body/sections/about-me/AboutMe';
import Education from '../body/sections/education/Education';
import Projects from '../body/sections/projects/Projects';

const Body = () => {
    const { pathname } = useLocation();

    useEffect(() => {
        document.getElementById("app").scrollTo({ top: 0, behavior: "smooth" });
    }, [pathname]);

    return (
        <div className={"body"}>
            <Container>
                    <Switch>
                        <Route exact path={"/"}>
                            <Home />
                        </Route>
                        <Route exact path={"/about-me"}>
                            <AboutMe />
                        </Route>
                        <Route exact path={"/education"}>
                            <Education />
                        </Route>
                        <Route exact path={"/projects"}>
                            <Projects />
                        </Route>
                        <Route>
                            <Redirect to={"/"} />
                        </Route>
                    </Switch>
            </Container>
        </div>
    );
};

export default Body;
