import React from 'react';

import { FormattedMessage } from 'react-intl';

import Title from '../../title/Title';
import Row from '../../row/Row';
import Paragraph from '../../paragraph/Paragraph';
import CustomLink from '../../link/CustomLink';

const Education = () => {
    return (
        <div>
            <Title id={"body.education.title"} />
            <Row>
                <Paragraph>
                    <FormattedMessage id={"body.education.text-1"} />
                    <CustomLink to={"https://epita.fr"} id={"body.education.text-2"} />
                    <FormattedMessage id={"body.education.text-3"} />
                    <CustomLink to={"/projects"} id={"body.education.text-4"} isIntern />
                    <FormattedMessage id={"body.education.text-5"} />
                    <br/><br/>
                    <FormattedMessage id={"body.education.text-6"} />
                    <CustomLink to={"https://www.griffith.ie"} id={"body.education.text-7"} />
                    <FormattedMessage id={"body.education.text-8"} />
                    <br/><br/>
                    <FormattedMessage id={"body.education.text-9"} />
                    <CustomLink to={"https://epita.fr"} id={"body.education.text-10"} />
                    <FormattedMessage id={"body.education.text-11"} />
                    <br/><br/>
                    <FormattedMessage id={"body.education.text-12"} />
                    <CustomLink to={"https://www.epita.fr/nos-formations/diplome-ingenieur/cycle-ingenieur/les-majeures#majeure-MTI"} id={"body.education.text-13"} />
                    <FormattedMessage id={"body.education.text-14"} />
                </Paragraph>
            </Row>
        </div>
    );
};

export default Education;
