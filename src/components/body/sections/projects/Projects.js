import React from 'react';

import { FormattedMessage } from 'react-intl';

import Title from '../../title/Title';
import Row from '../../row/Row';
import Paragraph from '../../paragraph/Paragraph';
import ParagraphTitle from '../../paragraph/paragraph-title/ParagraphTitle';
import CustomLink from "../../link/CustomLink";

const Projects = () => {
    return (
        <div>
            <Title id={"body.projects.title"} />
            <Row>
                <Paragraph>
                    <ParagraphTitle id={"body.projects.plic.title"}/>
                    <FormattedMessage id={"body.projects.plic.text-1"} />
                    <br/><br/>
                    <FormattedMessage id={"body.projects.plic.text-2"} />
                    <br/>
                    <FormattedMessage id={"body.projects.plic.text-3"} />
                    <br/>
                    <FormattedMessage id={"body.projects.plic.text-4"} />
                    <br/>
                    <FormattedMessage id={"body.projects.plic.text-5"} />
                    <br/>
                    <FormattedMessage id={"body.projects.plic.text-6"} />
                    <br/><br/>
                    <FormattedMessage id={"body.projects.plic.text-7"} />
                    <br/>
                    <FormattedMessage id={"body.projects.plic.text-8"} />
                    <br/>
                    <FormattedMessage id={"body.projects.plic.text-9"} />
                    <br/>
                    <FormattedMessage id={"body.projects.plic.text-10"} />
                    <br/><br/>
                    <FormattedMessage id={"body.projects.plic.text-11"} />
                    <br/><br/>
                    <CustomLink to={"https://front-end-learn-run.herokuapp.com"} id={"body.projects.plic.link"} />
                </Paragraph>
            </Row>
            <Row>
                <Paragraph>
                    <ParagraphTitle id={"body.projects.cloudvault.title"} />
                    <FormattedMessage id={"body.projects.cloudvault.text-1"} />
                    <br/>
                    <FormattedMessage id={"body.projects.cloudvault.text-2"} />
                    <br/>
                    <FormattedMessage id={"body.projects.cloudvault.text-3"} />
                    <br/>
                    <FormattedMessage id={"body.projects.cloudvault.text-4"} />
                    <br/>
                    <FormattedMessage id={"body.projects.cloudvault.text-5"} />
                    <br/>
                    <FormattedMessage id={"body.projects.cloudvault.text-6"} />
                    <br/>
                    <FormattedMessage id={"body.projects.cloudvault.text-7"} />
                    <br/>
                    <FormattedMessage id={"body.projects.cloudvault.text-8"} />
                    <br/><br/>
                    <FormattedMessage id={"body.projects.cloudvault.text-9"} />
                </Paragraph>
                <Paragraph>
                    <ParagraphTitle id={"body.projects.yakastar.title"} />
                    <FormattedMessage id={"body.projects.yakastar.text-1"} />
                    <br/>
                    <FormattedMessage id={"body.projects.yakastar.text-2"} />
                    <br/>
                    <FormattedMessage id={"body.projects.yakastar.text-3"} />
                    <br/>
                    <FormattedMessage id={"body.projects.yakastar.text-4"} />
                    <br/>
                    <FormattedMessage id={"body.projects.yakastar.text-5"} />
                    <br/>
                    <FormattedMessage id={"body.projects.yakastar.text-6"} />
                    <br/>
                    <FormattedMessage id={"body.projects.yakastar.text-7"} />
                    <br/><br/>
                    <FormattedMessage id={"body.projects.yakastar.text-8"} />
                </Paragraph>
            </Row>
            <Row>
                <Paragraph>
                    <ParagraphTitle id={"body.projects.chess.title"} />
                    <FormattedMessage id={"body.projects.chess.text-1"} />
                </Paragraph>
                <Paragraph>
                    <ParagraphTitle id={"body.projects.42sh.title"} />
                    <FormattedMessage id={"body.projects.42sh.text-1"} />
                    <br/>
                    <FormattedMessage id={"body.projects.42sh.text-2"} />
                    <br/>
                    <FormattedMessage id={"body.projects.42sh.text-3"} />
                    <br/>
                    <FormattedMessage id={"body.projects.42sh.text-4"} />
                    <br/>
                    <FormattedMessage id={"body.projects.42sh.text-5"} />
                </Paragraph>
            </Row>
        </div>
    );
};

export default Projects;
