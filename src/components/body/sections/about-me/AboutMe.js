import React from 'react';

import { FormattedMessage } from 'react-intl';

import Title from '../../title/Title';
import Row from '../../row/Row';
import Paragraph from '../../paragraph/Paragraph';
import ParagraphTitle from '../../paragraph/paragraph-title/ParagraphTitle';
import CustomLink from '../../link/CustomLink';

const AboutMe = () => {
    return (
        <div>
            <Title id={"body.about-me.presentation.title"} />
            <Row>
                <Paragraph>
                    <FormattedMessage id={"body.about-me.presentation.text-1"} />
                    <CustomLink to={"https://epita.fr"} id={"body.about-me.presentation.text-2"} />
                    <FormattedMessage id={"body.about-me.presentation.text-3"} />
                    <br/><br/>
                    <FormattedMessage id={"body.about-me.presentation.text-4"} />
                    <br/><br/>
                    <FormattedMessage id={"body.about-me.presentation.text-5"} />
                    <CustomLink to={"/download/CV - Antonin Ginet.pdf"} id={"body.about-me.presentation.text-6"} />
                    <FormattedMessage id={"body.about-me.presentation.text-7"} />
                    <CustomLink to={"mailto:antonin.ginet98@gmail.com"} id={"body.about-me.presentation.text-8"} />
                    <FormattedMessage id={"body.about-me.presentation.text-9"} />
                    <CustomLink to={"https://linkedin.com/in/antonin-ginet"} id={"body.about-me.presentation.text-10"} />
                    <FormattedMessage id={"body.about-me.presentation.text-11"} />
                </Paragraph>
            </Row>
            <Title id={"body.about-me.experiences.title"} />
            <Row>
                <Paragraph>
                    <ParagraphTitle id={"body.about-me.experiences.jcdecaux.title"}/>
                    <FormattedMessage id={"body.about-me.experiences.jcdecaux.text-1"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.jcdecaux.text-2"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.jcdecaux.text-3"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.jcdecaux.text-4"} />
                    <br/><br/>
                    <FormattedMessage id={"body.about-me.experiences.jcdecaux.text-5"} />
                    <br/><br/>
                    <FormattedMessage id={"body.about-me.experiences.jcdecaux.text-6"} />
                </Paragraph>
            </Row>
            <Row>
                <Paragraph>
                    <ParagraphTitle id={"body.about-me.experiences.wediagnostix.title"} />
                    <FormattedMessage id={"body.about-me.experiences.wediagnostix.text-1"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.wediagnostix.text-2"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.wediagnostix.text-3"} />
                    <br/><br/>
                    <FormattedMessage id={"body.about-me.experiences.wediagnostix.text-4"} />
                </Paragraph>
                <Paragraph>
                    <ParagraphTitle id={"body.about-me.experiences.karetis.title"} />
                    <FormattedMessage id={"body.about-me.experiences.karetis.text-1"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.karetis.text-2"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.karetis.text-3"} />
                    <br/><br/>
                    <FormattedMessage id={"body.about-me.experiences.karetis.text-4"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.karetis.text-5"} />
                    <br/><br/>
                    <FormattedMessage id={"body.about-me.experiences.karetis.text-6"} />
                </Paragraph>
            </Row>
            <Row>
                <Paragraph>
                    <ParagraphTitle id={"body.about-me.experiences.skopai.title"} />
                    <FormattedMessage id={"body.about-me.experiences.skopai.text-1"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.skopai.text-2"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.skopai.text-3"} />
                    <br/>
                    <FormattedMessage id={"body.about-me.experiences.skopai.text-4"} />
                    <br/><br/>
                    <FormattedMessage id={"body.about-me.experiences.skopai.text-5"} />
                </Paragraph>
                <Paragraph>
                    <ParagraphTitle id={"body.about-me.experiences.psa.title"} />
                    <FormattedMessage id={"body.about-me.experiences.psa.text-1"} />
                    <br/><br/>
                    <FormattedMessage id={"body.about-me.experiences.psa.text-2"} />
                </Paragraph>
            </Row>
        </div>
    );
};

export default AboutMe;
