import React from 'react';

import { FormattedMessage } from 'react-intl';

import Title from '../../title/Title';
import Row from '../../row/Row';
import Paragraph from '../../paragraph/Paragraph';
import CustomLink from '../../link/CustomLink';

const Home = () => {
    return (
        <div>
            <Title id={"body.home.title"} />
            <Row>
                <Paragraph>
                    <FormattedMessage id={"body.home.text-1-1"} />
                    <br/><br/>
                    <FormattedMessage id={"body.home.text-1-2"} />
                    <CustomLink to={"/about-me"} isIntern id={"body.home.text-1-3"} />
                    <FormattedMessage id={"body.home.text-1-4"} />
                    <br/><br/>
                    <FormattedMessage id={"body.home.text-1-5"} />
                    <CustomLink to={"https://epita.fr"} id={"body.home.text-1-6"} />
                    <FormattedMessage id={"body.home.text-1-7"} />
                    <CustomLink to={"/education"} isIntern id={"body.home.text-1-8"} />
                    <FormattedMessage id={"body.home.text-1-9"} />
                    <br/><br/>
                    <FormattedMessage id={"body.home.text-1-10"} />
                    <CustomLink to={"/projects"} isIntern id={"body.home.text-1-11"} />
                    <FormattedMessage id={"body.home.text-1-12"} />
                    <br/><br/>
                    <FormattedMessage id={"body.home.text-1-13"} />
                    <CustomLink to={"https://linkedin.com/in/antonin-ginet"} id={"body.home.text-1-14"} />
                    <FormattedMessage id={"body.home.text-1-15"} />
                    <CustomLink to={"https://gitlab.com/Ryubi98"} id={"body.home.text-1-16"} />
                    <FormattedMessage id={"body.home.text-1-17"} />
                    <CustomLink to={"https://github.com/Ryubi98"} id={"body.home.text-1-18"} />
                    <FormattedMessage id={"body.home.text-1-19"} />
                </Paragraph>
                <Paragraph>
                    <FormattedMessage id={"body.home.text-2-1"} />
                    <CustomLink to={"https://fr.reactjs.org"} id={"body.home.text-2-2"} />
                    <FormattedMessage id={"body.home.text-2-3"} />
                    <CustomLink to={"https://gitlab.com/Ryubi98/portfolio"} id={"body.home.text-2-4"} />
                    <FormattedMessage id={"body.home.text-2-5"} />
                    <br/><br/>
                    <FormattedMessage id={"body.home.text-2-6"} />
                    <CustomLink to={"https://www.npmjs.com/package/react-intl"} id={"body.home.text-2-7"} />
                    <FormattedMessage id={"body.home.text-2-8"} />
                    <br/><br/>
                    <FormattedMessage id={"body.home.text-2-9"} />
                    <br/>
                    <FormattedMessage id={"body.home.text-2-10"} />
                    <br/><br/>
                    <FormattedMessage id={"body.home.text-2-11"} />
                    <CustomLink to={"https://www.heroku.com"} id={"body.home.text-2-12"} />
                    <FormattedMessage id={"body.home.text-2-13"} />
                    <br/>
                    <FormattedMessage id={"body.home.text-2-14"} />
                    <CustomLink to={"https://docs.gitlab.com/ee/ci"} id={"body.home.text-2-15"} />
                    <FormattedMessage id={"body.home.text-2-16"} />
                </Paragraph>
            </Row>
        </div>
    );
};

export default Home;
