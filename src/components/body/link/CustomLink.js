import React from 'react';
import './CustomLink.css';

import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

const CustomLink = ({ to, isIntern, id }) => {
    return (
        isIntern ?
            (<Link className={"body-link"} to={to}>
                <FormattedMessage id={id} />
            </Link>)
        :
            (<a className={"body-link"} href={to} target={"_newtab"}>
                <FormattedMessage id={id} />
            </a>)
    );
};

export default CustomLink;