import React from 'react';
import './Title.css';

import { FormattedMessage } from 'react-intl';

const Title = ({ id }) => {
    return (
        <h2 className={"body-title"}>
            <FormattedMessage id={id} />
        </h2>
    );
};

export default Title;