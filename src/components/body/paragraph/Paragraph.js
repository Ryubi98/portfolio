import React from 'react';
import './Paragraph.css';

const Paragraph = ({ children }) => {
    return (
        <div className={"body-paragraph"}>
            {children}
        </div>
    );
};

export default Paragraph;