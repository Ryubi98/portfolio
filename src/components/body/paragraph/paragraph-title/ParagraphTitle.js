import React from 'react';
import './ParagraphTitle.css';

import { FormattedMessage } from 'react-intl';

const ParagraphTitle = ({ id }) => {
    return (
        <h2 className={"paragraph-title"}>
            <FormattedMessage id={id} />
        </h2>
    );
};

export default ParagraphTitle;
